import re


###### set these values ######
NEGATIVE = True
FILENAME = "neg.csv"

EVALUATORS = ["EE","EE","EE","EE"]
##############################



POSITIVE = not NEGATIVE

def parse_csv_line(csv_line):
    # Regular expression to split the line into fields, considering quoted fields
    pattern = r'"[^"]*"|[^,]+'
    csv_line = csv_line.replace('""', 'ESCAPED_CHAR')
    for i in range(4): # replaces ,,, to , , , for up to 5 commas
        csv_line = csv_line.replace(',,', ', ,')
    fields = re.findall(pattern, csv_line)
    fields = [field.replace('ESCAPED_CHAR', '"') for field in fields]
    # Remove the double quotes from the start and end of each field
    fields = [field.strip('"') for field in fields]
    return fields

def parse_csv_file():
    # parsing the file
    lines = [line.strip("\n") for line in open(FILENAME).readlines()]
    lines = lines[4:]
    lines = [parse_csv_line(line) for line in lines]

    # combining lines split by enter in text
    merged_lines = []
    current_line = []

    for line in lines:
        if not current_line:
            current_line = line
        else:
            current_line[-1] += line[0]
            current_line += line[1:]

        if ((len(current_line) >= 17 and NEGATIVE) or (len(current_line) >= 15 and POSITIVE)): #or i >= len(lines) - 2:
            merged_lines.append(current_line)
            current_line = []

    if current_line:
        merged_lines.append(current_line)

    return merged_lines


def construct_table_entries_string(lines):
    entries = []
    for line in lines:
        entries.append(f"""    <tr>
        <td>{line[0]}</td>
        <td>{line[1]}</td>
        <td><p>{line[2]}</p></td>
        <td><a href="videos/{line[3]}">{line[3]}</a></td>
        <td>{line[5]}</td>""" + (f"""
        <td>{line[6] if line[6] == "y" else ""}</td>
        <td>{line[7] if line[7] == "y" else ""}</td>
        <td>{line[8] if line[8] == "y" else ""}</td>
        <td>{line[9] if line[9] == "y" else ""}</td>
        <td>{line[10]}</td>
        <td>{line[11]}</td>
        <td>{line[12]}</td>
        <td>{line[13]}</td>
        <td>{line[14]}</td>
    </tr>""" if POSITIVE else f"""
        <td>{line[6]}</td>
        <td>{line[7]}</td>
        <td>{line[8] if line[8] == "y" else ""}</td>
        <td>{line[9] if line[9] == "y" else ""}</td>
        <td>{line[10] if line[10] == "y" else ""}</td>
        <td>{line[11] if line[11] == "y" else ""}</td>
        <td>{line[12]}</td>
        <td>{line[13]}</td>
        <td>{line[14]}</td>
        <td>{line[15]}</td>
        <td>{line[16]}</td>
    </tr>"""))
    return "\n".join(entries)


def construct_table_string(entries_string):
    table = f'''<table class="{'positives-table' if POSITIVE else 'problems-table'}">
    <thead>
        <tr>
            <th rowspan="2">No.</th>
            <th rowspan="2">Title</th>
            <th rowspan="2">Description</th>
            <th rowspan="2">Video Clip(s)</th>
            <th rowspan="2">How Reproducible?</th>{"""
            <th rowspan="2">Heuristic</th>
            <th rowspan="2">Only When</th>""" if not POSITIVE else ''}
            <th colspan="4">Found By</th>
            <th colspan="5">{"Positivity" if POSITIVE else "Severity"}</th>
        </tr>

        <tr>
            <th>{EVALUATORS[0]}</th>
            <th>{EVALUATORS[1]}</th>
            <th>{EVALUATORS[2]}</th>
            <th>{EVALUATORS[3]}</th>

            <th>{EVALUATORS[0]}</th>
            <th>{EVALUATORS[1]}</th>
            <th>{EVALUATORS[2]}</th>
            <th>{EVALUATORS[3]}</th>
            <th>Mean</th>
        </tr>
    </thead>


    <tbody>

{entries_string}

    </tbody>
</table>
'''
    return table


if __name__ == "__main__":
    lines = parse_csv_file()
    table_entries = construct_table_entries_string(lines)
    table = construct_table_string(table_entries)
    with open("out.html", "w") as f:
        f.write(table)

