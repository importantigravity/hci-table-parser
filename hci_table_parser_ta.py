import re


###### set these values ######
NEGATIVE = True
FILENAME = "neg.csv"

EVALUATORS = ["EE","EE","EE","EE"]
CSV_SEPERATOR = ";"
VIDEO_CLIPS_SEPERATOR = ", "
##############################

html_save = {">": "&gt;",
             "<": "&lt;"}

PATTERN = r'"[^"]*"|[^' + CSV_SEPERATOR + r']+'
POSITIVE_LINE_LENGTH = 11
NEGATIVE_LINE_LENGTH = 11
POSITIVE = not NEGATIVE

def parse_csv_line(csv_line):
    # Regular expression to split the line into fields, considering quoted fields
    csv_line = csv_line.replace('""', 'ESCAPED_CHAR')
    for i in range(4): # replaces ,,, to , , , for up to 5 commas
        csv_line = csv_line.replace(2*CSV_SEPERATOR, CSV_SEPERATOR + ' ' + CSV_SEPERATOR)
    fields = re.findall(PATTERN, csv_line)
    fields = [field.replace('ESCAPED_CHAR', '"') for field in fields]
    # Replace all characters in the html_save dictionary fith their escape counterparts
    for char, escaped_char in html_save.items():
        fields = [field.replace(char, escaped_char) for field in fields]
    # Remove the double quotes from the start and end of each field
    fields = [field.strip('"') for field in fields]
    return fields

def parse_csv_file():
    # parsing the file
    lines = [line.strip("\n") for line in open(FILENAME).readlines()]
    lines = lines[4:]
    lines = [parse_csv_line(line) for line in lines]
    lines = [line for line in lines if line != []]

    # combining lines split by enter in text
    merged_lines = []
    current_line = []

    for line in lines:
        if not current_line:
            current_line = line
        else:
            current_line[-1] += line[0]
            current_line += line[1:]

        if ((len(current_line) >= NEGATIVE_LINE_LENGTH and NEGATIVE) or
            (len(current_line) >= POSITIVE_LINE_LENGTH and POSITIVE)):
            merged_lines.append(current_line)
            current_line = []

    if current_line:
        merged_lines.append(current_line)

    return merged_lines


def construct_table_entries_string(lines):
    entries = []
    for line in lines:
        entries.append(f"""        <tr>
            <td>{line[0]}</td>
            <td>{line[1]}</td>
            <td><p>{line[2]}</p></td>
            <td>{" ".join([f'<a href="videos/{vid}">{vid}</a>' for vid in line[3].split(VIDEO_CLIPS_SEPERATOR)])}</td>
            <td>{line[4]}</td>
            <td>{line[5]}</td>
            <td>{line[6]}</td>
            <td>{line[7]}</td>
            <td>{line[8]}</td>
            <td>{line[9]}</td>
            <td>{line[10]}</td>
        </tr>""")
    return "\n".join(entries)


def construct_table_string(entries_string):
    table = f'''<table class="{'positives-table' if POSITIVE else 'problems-table'}">
    <thead>
        <tr>
            <th rowspan="2">No.</th>
            <th rowspan="2">Title</th>
            <th rowspan="2">Description</th>
            <th rowspan="2">Video Clip(s)</th>
            <th rowspan="2">Timestamps(s)</th>
            <th rowspan="2">Location (how reproducible?)</th>
            <th colspan="5">{"Positivity" if POSITIVE else "Severity"}</th>
        </tr>

        <tr>
            <th>{EVALUATORS[0]}</th>
            <th>{EVALUATORS[1]}</th>
            <th>{EVALUATORS[2]}</th>
            <th>{EVALUATORS[3]}</th>
            <th>Mean</th>
        </tr>
    </thead>

    <tbody>

{entries_string}

    </tbody>
</table>'''
    return table


if __name__ == "__main__":
    lines = parse_csv_file()
    table_entries = construct_table_entries_string(lines)
    table = construct_table_string(table_entries)
    with open(f"{'.'.join(FILENAME.split('.')[:-1])}.html", "w") as f:
        f.write(table)

